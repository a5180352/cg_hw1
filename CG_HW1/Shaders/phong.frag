#version 400

in vec3 v_normal;
in vec2 v_texcoord;
in vec3 light_dir;
in vec3 view_dir;

uniform sampler2D MyTexture_1;

// there should be a out vec4 in fragment shader defining the output color of fragment shader(variable name can be arbitrary)
out vec4 outColor;

void main() {
	vec4 light_color = vec4(1.0, 1.0, 1.0, 1.0);
	// ambient
	vec4 ambient = 0.2 * light_color;
	// diffuse
	vec3 n_light_dir = normalize(light_dir);
	vec3 n_normal    = normalize(v_normal);
	float diff = max(dot(n_normal, n_light_dir), 0.0);
	vec4 diffuse = diff * (0.5 * light_color);
	// specular
	vec3 n_view_dir = normalize(view_dir);
	vec3 reflec_dir = reflect(-n_light_dir, n_normal);
	float spec = pow(max(dot(n_view_dir, reflec_dir), 0.0), 3.6);
	vec4 specular = spec * (0.8 * light_color);
	// final
	vec4 objectColor = texture2D(MyTexture_1, v_texcoord);
	outColor = (ambient + diffuse + specular) * objectColor;
}