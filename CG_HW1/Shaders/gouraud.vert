#version 400

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;

// M, P, MV matrix
uniform mat4 M;
uniform mat4 P;
uniform mat4 MV;

uniform vec3 light_pos; // diffuse & specular
uniform vec3 view_pos;  // specular

out vec2 v_texcoord;
out vec4 lighting;

void main() {
	vec3 pos = (M * vec4(position ,1.0)).xyz;
	gl_Position = P * MV * vec4(position , 1.0);

	vec3 v_normal = mat3(transpose(inverse(M))) * normal; // scale normal vector correctly
	v_texcoord = texcoord;

	vec3 light_dir = light_pos - pos;
	vec3 view_dir  = view_pos  - pos;
	vec4 light_color = vec4(1.0, 1.0, 1.0, 1.0);
	// ambient
	vec4 ambient = 0.2 * light_color;
	// diffuse
	vec3 n_light_dir = normalize(light_dir);
	vec3 n_normal    = normalize(v_normal);
	float diff = max(dot(n_normal, n_light_dir), 0.0);
	vec4 diffuse = diff * (0.5 * light_color);
	// specular
	vec3 n_view_dir = normalize(view_dir);
	vec3 reflec_dir = reflect(-n_light_dir, n_normal);
	float spec = pow(max(dot(n_view_dir, reflec_dir), 0.0), 3.6);
	vec4 specular = spec * (0.8 * light_color);

	lighting = (ambient + diffuse + specular);
}