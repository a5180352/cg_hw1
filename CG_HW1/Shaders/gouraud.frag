#version 400

in vec2 v_texcoord;
in vec4 lighting;

uniform sampler2D MyTexture_1;

// there should be a out vec4 in fragment shader defining the output color of fragment shader(variable name can be arbitrary)
out vec4 outColor;

void main() {
	// final
	vec4 objectColor = texture2D(MyTexture_1, v_texcoord);
	outColor = lighting * objectColor;
}