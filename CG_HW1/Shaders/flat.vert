#version 400

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;

// M, P, MV matrix
uniform mat4 M;
uniform mat4 P;
uniform mat4 MV;

uniform vec3 light_pos; // diffuse & specular
uniform vec3 view_pos;  // specular

flat out vec3 v_normal;
out vec2 v_texcoord;
out vec3 light_dir;
out vec3 view_dir;

void main() {
	vec3 pos = (M * vec4(position ,1.0)).xyz;
	gl_Position = P * MV * vec4(position , 1.0);

	v_normal = mat3(transpose(inverse(M))) * normal; // scale normal vector correctly
	v_texcoord = texcoord;
	light_dir = light_pos - pos;
	view_dir  = view_pos  - pos;
}